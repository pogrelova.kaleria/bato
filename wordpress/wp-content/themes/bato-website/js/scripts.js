const sliderItems = document.querySelectorAll('.slider-item-name');
const card = document.querySelectorAll('.item-card');
const sectionBanner = document.querySelector('.section-banner');
const menuMainItems = document.querySelectorAll('.header-menu .menu-item a');
const cardTitleItems = document.querySelectorAll('.section-cards .card-title');
const socialMedia = document.querySelectorAll('.social-media .menu-item a');
const subtitleTeg = document.querySelector('.subtitle');
const subTitleArr = JSON.parse(subTitles);
const subTitleArrForAnimation = subTitleArr.map(item => item.subtitle);
const preloaderBlock = document.querySelector('.preloader');
const page = document.querySelector('.page');
const sectionAnimation = document.querySelector(".section-animation");
const sectionNumbersNumber = document.querySelectorAll('.section-numbers__number');
const linkContactPhrase = document.querySelector('.section-contact__phrase a');
const linkContactPhraseContent = linkContactPhrase.innerHTML;
const linkContactPhraseLink = linkContactPhrase.getAttribute('href');
const teamItemContainer = document.querySelectorAll('.container');
const frontCube = document.querySelectorAll('.front-cube');
const backCube = document.querySelectorAll('.back-cube');
const leftCube = document.querySelectorAll('.left-cube');
const rightCube = document.querySelectorAll('.right-cube');
const bottomCube = document.querySelectorAll('.bottom-cube');
const topCube = document.querySelectorAll('.top-cube');
const cubeItem = document.querySelectorAll('.cube');
const cursorFollowArr = document.querySelectorAll('.cursor-follow-item');
const cursorWrapper = document.querySelector('.cursor-follow');
const speedSlider = 1;
const iconOpenNav = document.querySelector('.navigation-icon');
const iconCloseNav = document.querySelector('.navigation-icon-close');
const navBlockHeader = document.querySelector('.header__navigation');
const menuItem = document.querySelectorAll('.header-menu .menu-item')
const preloaderMenuMob = document.querySelector('.preloader-menu-mob');
const header = document.querySelector('header')
const scrollChange = 1
let teamCube = document.querySelector('.section-team__items-wrapp');
let teamCubes = document.querySelectorAll('.section-team__items-wrapp');
let scrollCount = 0;
let isScrolling;
let scrollpos = window.scrollY;
let teamItemWidth;
let positionCursor = { x: 0, y: 0 };
let positionMouse = { x: 0, y: 0 };

// preloader
if(preloaderBlock) { 
    window.addEventListener('load', ()=> {
        preloaderBlock.classList.add('preloader-start');
        setTimeout(()=>{
            page.classList.add('visible');
            // preloaderBlock.classList.add('preloader-close');
        }, 2800);
    });
}

// add border in header mobile
window.addEventListener('scroll', function() {
    scrollpos = window.scrollY;

    if (scrollpos >= scrollChange) { 
        header.classList.add('header-mobile-border')
    } else { 
        header.classList.remove('header-mobile-border')
    }
})

// open nav
iconOpenNav.addEventListener('click', ()=> {
    navBlockHeader.classList.add('open-menu');
    page.classList.remove('visible');
    preloaderMenuMob.classList.add('preloader-menu-mob-start');
})

// close nav
iconCloseNav.addEventListener('click', ()=> {
    navBlockHeader.classList.remove('open-menu');
    page.classList.add('visible');
    preloaderMenuMob.classList.remove('preloader-menu-mob-start');
})

// close menu on click menu item
menuItem.forEach((el)=> {
    el.addEventListener('click', ()=> {
        navBlockHeader.classList.remove('open-menu');

        if(!page.classList.contains('visible')) {
            page.classList.add('visible');
        }
        if(preloaderMenuMob.classList.contains('preloader-menu-mob-start')) {
            preloaderMenuMob.classList.remove('preloader-menu-mob-start');
        }
    })
})

// animation wave
function animationWave(arr) {
    arr.forEach(function(el) {
        let item = el.innerHTML;
        let itemInSpan = item.split('').reduce( (res, char, index) => `${res}<span style="--index: ` + `${index}` +`">${char}</span>`, '')
    
        el.innerHTML = `
        <div class="animation-wrapp-wave">
            <div class="wave-item">`
                + itemInSpan +
            `</div>
            <div class="wave-item-clone">`
                + itemInSpan +
            `</div>
        </div>`;
    })
}

// animation subtitle
class TextScramble {
    constructor(el) {
        this.el = el
        this.chars = '!\\/?#abdehkmnpswxz'
        this.update = this.update.bind(this)
    }
    setText(newText) {
        const oldText = this.el.innerText
        const length = Math.max(oldText.length, newText.length)
        const promise = new Promise((resolve) => this.resolve = resolve)
        this.queue = []
        for (let i = 0; i < length; i++) {
            const from = oldText[i] || ''
            const to = newText[i] || ''
            const start = Math.floor(Math.random() * 40)
            const end = start + Math.floor(Math.random() * 40)
            this.queue.push({ from, to, start, end })
        }
        cancelAnimationFrame(this.frameRequest)
        this.frame = 0
        this.update()
        return promise
    }
    update() {
        let output = ''
        let complete = 0
        for (let i = 0, n = this.queue.length; i < n; i++) {
            let { from, to, start, end, char } = this.queue[i]
            if (this.frame >= end) {
                complete++
                output += to
                } else if (this.frame >= start) {
                if (!char || Math.random() < 0.28) {
                    char = this.randomChar()
                    this.queue[i].char = char
                }
                output += '<span class="dud-text">'+ char +'</span>'
                } else {
                output += from
            }
        }
        this.el.innerHTML = output
        if (complete === this.queue.length) {
            this.resolve()
            } else {
            this.frameRequest = requestAnimationFrame(this.update)
            this.frame++
        }
    }
    randomChar() {
        return this.chars[Math.floor(Math.random() * this.chars.length)]
    }
}

setTimeout(()=>{
    const fx = new TextScramble(subtitleTeg)
    let counter = 0
    const next = () => {
        fx.setText(subTitleArrForAnimation[counter]).then(() => {
            setTimeout(next, 3000)
        })
        counter = (counter + 1) % subTitleArrForAnimation.length
    }
    next()
}, 6000)

//  for video for Mozilla Firefox, Safari
if(navigator.userAgent.indexOf("Safari") != -1 && navigator.userAgent.indexOf("Chrome") === -1) {
    page.classList.add('body-safari');
} else if(navigator.userAgent.indexOf("Firefox") != -1 ){
    page.classList.add('body-firefox');
}

if (!document.querySelector('body').classList.contains('body-safari')) {
    animationWave(cardTitleItems);
    animationWave(menuMainItems);
    animationWave(socialMedia);
}

// for splide slide
const splide = new Splide( '.splide', {
    updateOnMove: true,
    type   : 'loop',
    focus  : 'center',
    gap: 100,
    autoWidth: true,
    arrows: false,
    pagination: false,
    drag: false,
    autoScroll: {
        speed: speedSlider,
        pauseOnHover: false,
        pauseOnFocus: false
    },
    breakpoints: {
		992: {
			gap: 55,
		},
    }
} );

splide.mount( window.splide.Extensions );

// section numbers
if(sectionNumbersNumber) {

    sectionNumbersNumber.forEach((el)=> {

        let itemInSpan = el.innerHTML.split('').reduce( (res, char, index) => `${res}<span style="animation-delay: ${1 + index / 3}s;">${char}</span>`, '');
        el.innerHTML = itemInSpan;
    })
}

// section team cards
let root = document.documentElement;
if(teamItemContainer) {
    teamItemWidth =  teamCube.offsetWidth;
    
    root.style.setProperty('--cube-width', teamItemWidth + "px");

    root.style.setProperty('--cube-width-minus', (-teamItemWidth) + "px");
    let teamItemWidthHalf = teamItemWidth / 2;

    let arr = ['rotateY(90deg)', 'rotateY(-90deg)', 'rotateX(-90deg)', 'rotateX(90deg)'];

    function arrayRandElement(arr) {
        let random = Math.floor(Math.random() * arr.length);
        return arr[random];
    }

    cubeItem.forEach((el)=> {

        el.style.transform = `translateZ(-${teamItemWidthHalf}px) rotateY(0deg)`;

        el.addEventListener('click', function test() {
            if (el.style.transform == `translateZ(-${teamItemWidthHalf}px) rotateY(0deg)`) {
                el.style.transform = `translateZ(-${teamItemWidth}px) ${arrayRandElement(arr)}`;
            } else {
                el.style.transform = `translateZ(-${teamItemWidthHalf}px) rotateY(0deg)`;
            }
        })
    })

    window.addEventListener('resize', ()=> {
   
        teamItemWidth = teamCube.offsetWidth;
        teamItemWidthMinus = -teamItemWidth;

        root.style.setProperty('--cube-width', teamItemWidth + "px");
        root.style.setProperty('--cube-width-minus', (-teamItemWidth) + "px");
    })
}

if (!document.querySelector('body').classList.contains('body-safari')){
    // magnetic-btn
    if(linkContactPhrase) {

        linkContactPhrase.innerHTML = `
            <span class="container container-link">
                <span class="magnetic magnetic-link">
                    <span class="btn btn-outline-primary magnet-button">${linkContactPhraseContent}
                    </span>
                </span>
            </span>`;

        let magnets = document.querySelectorAll('.magnetic')
        let strength = 50

        if(magnets){
            magnets.forEach( (magnet) => {
        magnet.addEventListener('mousemove', moveMagnet );
        magnet.addEventListener('mouseout', function(event) {
            TweenMax.to( event.currentTarget, 1, {x: 0, y: 0, ease: Power4.easeOut})
        } );
            });

            function moveMagnet(event) {
            var magnetButton = event.currentTarget
            var bounding = magnetButton.getBoundingClientRect()

            TweenMax.to( magnetButton, 1, {
                x: ((( event.clientX - bounding.left)/magnetButton.offsetWidth) - 0.5) * strength,
                y: ((( event.clientY - bounding.top)/magnetButton.offsetHeight) - 0.5) * strength,
                ease: Power4.easeOut
            })
            }
        }
    }

    function upSpeedSlider() {
        scrollCount += 0.05;
            splide.options = {
                autoScroll: {
                    speed: speedSlider + scrollCount <= 5 ? speedSlider + scrollCount : 5,
                }
            };

        window.clearTimeout( isScrolling );

        isScrolling = setTimeout(function() {

            splide.options = {
                autoScroll: {
                    speed: speedSlider,
                }
            };

        }, 66);
    }
    
    function sliderSpeedOnScroll() {
        window.addEventListener('scroll', upSpeedSlider, false)
    }

    function defaultSpeedScrollSlider() {
        window.removeEventListener('scroll', upSpeedSlider, false)
    }
}

// ScrollMagic
// let controller = new ScrollMagic.Controller({addIndicators: true});
let controller = new ScrollMagic.Controller();

if (!document.querySelector('body').classList.contains('body-safari')){

    if(window.screen.width >= 1024) {
        let sectionSlider = new ScrollMagic.Scene({triggerElement: "#section-slider", duration: 1000, triggerHook: "onLeave", triggerHook: 1})
        // .addIndicators({name: "6 - section-slider"})
        .on('enter', sliderSpeedOnScroll)
        .on('leave', defaultSpeedScrollSlider)
        .addTo(controller)
        .reverse(true);
    }
}

let sectionAbout = new ScrollMagic.Scene({triggerElement: "#section-about", triggerHook: "onLeave", triggerHook: 0.8})
.setClassToggle(".section-about", "section-about-animation")
// .addIndicators({name: "1 - section-about"}) 
.addTo(controller)
.reverse(false);

let sectionCards = new ScrollMagic.Scene({triggerElement: "#section-cards", triggerHook: "onLeave", triggerHook: 0.8})
.setClassToggle(".section-cards", "section-cards-animation")
// .addIndicators({name: "1 - section-cards"}) 
.addTo(controller)
.reverse(false);

let sectionOffers = new ScrollMagic.Scene({triggerElement: "#section-offers", triggerHook: "onLeave", triggerHook: 0.8})
.setClassToggle(".section-offers", "section-offers-animation")
// .addIndicators({name: "2 - section-offers"}) 
.addTo(controller)
.reverse(false);

let sectionNumbers = new ScrollMagic.Scene({triggerElement: "#section-numbers", triggerHook: "onLeave", triggerHook: 0.8})
.setClassToggle(".section-numbers", "section-numbers-animation")
// .addIndicators({name: "4 - section-numbers"}) 
.addTo(controller)
.reverse(false);

let sectionTeam = new ScrollMagic.Scene({triggerElement: "#section-team", triggerHook: "onLeave", triggerHook: 0.8})
.setClassToggle(".section-team", "section-team-animation")
// .addIndicators({name: "5 - section-team"}) 
.addTo(controller)
.reverse(false);

let sectionContact = new ScrollMagic.Scene({triggerElement: "#section-contact", triggerHook: "onLeave", triggerHook: 0.8})
.setClassToggle(".section-contact", "section-contact-animation")
// .addIndicators({name: "6 - section-contact"}) 
.addTo(controller)
.reverse(false);

let footer = new ScrollMagic.Scene({triggerElement: "#footer", triggerHook: "onLeave", triggerHook: 0.9})
.setClassToggle(".footer", "footer-animation")
// .addIndicators({name: "6 - footer"}) 
.addTo(controller)
.reverse(false);

// for mobile
if(window.screen.width <= 1024) {
    // back to top
    let buttonToTop = document.getElementById("back-to-top");

    buttonToTop.addEventListener("click", backToTop);

    function backToTop() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    // for border mobile in card services
    let items = document.querySelectorAll('.card-services');

    for (let li of items) {
        li.addEventListener('click', function() {
            for (let li of items) {
                li.classList.remove('color-card');
            }
            this.classList.add('color-card');
        });
    }
}