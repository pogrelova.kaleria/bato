<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bato-website
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="body-wrapp">
<?php wp_body_open(); ?>

	<header id="masthead" class="header">
		<div class="header__inner main-size"> 
			<div class="logo">
					<?php
						the_custom_logo();
					?>
			</div>
			<nav id="header-navigation" class="header__navigation">
				<div class="navigation-bar">
					<div class="logo">
						<?php
							the_custom_logo();
						?>
					</div>
					<div class="navigation-icon-close">
						<span>close</span>
						<div>
							<img src="<?php echo get_template_directory_uri(); ?>/images/icon-close-nav-header.svg" alt="icon menu close">
						</div>
					</div>
				</div>
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-header',
							'menu_id'        => 'header-menu',
							'menu_class'	 => 'header-menu',
						)
					);
				?>
			</nav> 
			<div class="navigation-icon">
				<img src="<?php echo get_template_directory_uri(); ?>/images/menu-icon-header.svg" alt="icon menu">
			</div>
	</header>
